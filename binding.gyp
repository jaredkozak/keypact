{
    "targets": [{
        "target_name": "inputaddon",
        # 'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ],
		'conditions': [
			# [
            #     'OS == "mac"',
            #     {
			#         'sources': [
			# 	        'src_native/macos/macos-screen.mm',
			# 	        'src_native/common/screen.cc'
			#         ],
            #         'include_dirs': [
            #             'System/Library/Frameworks/ApplicationServices.framework/Headers',
            #             '<!@(node -p "require('node-addon-api').include")'
            #         ],
            #         'link_settings': {
            #             'libraries': [
            #                 '-framework ApplicationServices',
            #                 '-framework AppKit',
            #             ]
            #         }
			#     }
            # ],
			# [
            #     'OS == "linux"',
            #     {
            #         'link_settings': {
            #             'libraries': [
            #                 '-lX11',
            #             ]
            #         },
            #         'include_dirs': [
            #             '<!@(node -p "require('node-addon-api').include")'
            #         ],
            #         'sources': [
            #             'src_native/linux/linux-screen.cc',
            #             'src_native/linux/xdisplay.cc',
            #             'src_native/common/screen.cc',
            #         ]
            #     }
            # ],
			[
                "OS=='win'",
                {
                    'sources': [
                        'src_native/main.cc',
                        'src_native/win/windows-screen.cc',
                        'src_native/common/screen.cc',
                        'src_native/common/screenWrapper.cc',
                        'src_native/common/inputworker.cc'
                    ],
                    'include_dirs': [
                        '<!@(node -p "require(\'node-addon-api\').include")'
                    ],
                    'dependencies': [
                        '<!(node -p "require(\'node-addon-api\').gyp")'
                    ],
                    'ldflags': [
                            '-Wl,-rpath,<(module_root_dir)',
                    ],
                    # "cflags": [
                    #     "-std=c++11",
                    #     "-stdlib=libc++"
                    # ],
                    'defines': [
                        '_HAS_EXCEPTIONS=1'
                    ],
                    'msvs_settings': {
                        'VCCLCompilerTool': { 'ExceptionHandling': 1 },
                    },
                    'cflags!': [ '-fno-exceptions' ],
                    'cflags_cc!': [ '-fno-exceptions' ],
                    'xcode_settings': {
                        'GCC_ENABLE_CPP_EXCEPTIONS': 'YES',
                        'CLANG_CXX_LIBRARY': 'libc++',
                        'MACOSX_DEPLOYMENT_TARGET': '10.7',
                    }
			    }
            ]
        ]
    }]
}