import { ChildProcess, execSync, spawn } from 'child_process';
import * as gulp from 'gulp';
import { dest, parallel, series, watch } from 'gulp';
import * as ts from 'gulp-typescript';
import * as os from 'os';


const tsProject = ts.createProject('tsconfig.json', {
	declaration: true,
	isolatedModules: true,
});

let instance: ChildProcess;

function platform() {
    return os.platform();
}

// The `clean` function is not exported so it can be considered a private task.
// It can still be used within the `series()` composition.
function clean(cb) {
  // body omitted
  cb();
}

function killApp(cb) {
	if (instance) {
		instance.kill('SIGINT');
		instance = undefined;
	}

	if (cb) { cb() }
}

function restartApp(cb?) {
	if (instance) {
		instance.kill('SIGINT');
		instance = undefined;
	}

	const args = [ 'dist/index.js' ];
	if (process.argv.includes('--client')) {
		args.push('--config', 'config/keypact-client-config.json');
	}
	
	instance = spawn('node', args, {
		stdio: 'inherit',
		env: process.env,
	});

	if (cb) { cb() };

}

function buildTypescript() {
	return tsProject.src()
		.pipe(tsProject())
		.pipe(dest('dist'))

}

function buildCpp(cb) {
	execSync('npm run build:native', {
		env: {
			...process.env,
			JOBS: 'max',
		},
		stdio: 'inherit',
	});
	cb();
}

function build() {
	return parallel(
		buildCpp,
		buildTypescript,
	);
}

function watchAll() {
	watch('src', series([ 'build:ts', 'restartApp' ]));
	watch('src_native', series([ 'killApp', 'build:cpp', 'restartApp' ]));
}

let interval;

function watchDist() {
	restartApp();


	return watch([ 'dist', 'build' ], function() {
		// only restart with a delay of 1500 seconds
		if (interval !== undefined) {
			clearTimeout(interval);
			interval = undefined;
			return;
		}

		interval = setTimeout(() => {
			restartApp();
		}, 1500)
	});
}


gulp.task('build:cpp', buildCpp);
gulp.task('build:ts', buildTypescript);
gulp.task('build', build());
gulp.task('restartApp', restartApp);
gulp.task('killApp', killApp);
gulp.task('watch:dist',  watchDist);
gulp.task('watch', watchAll);

gulp.task('default', gulp.series(
	'build',
	'restartApp',
	'watch',
));
