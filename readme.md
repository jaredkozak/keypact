# Input Share Tool

## Requirements

Node 12

## Generate SSL certificates

https://stackoverflow.com/questions/10175812/how-to-create-a-self-signed-certificate-with-openssl

openssl req -x509 -newkey rsa:4096 -keyout config/test-key.pem -out config/test-cert.pem -days 365 -nodes -subj "/O=KeyPact/CN=Saphire"  -sha256

Copy to client:
cp ./config/test-cert.pem ../keypact-client/config/test-cert.pem

## Development

Start watching server with:

gulp --server

Once server is started, start watching dist folder for client with

gulp watch:dist --client


## References

https://medium.com/@atulanand94/beginners-guide-to-writing-nodejs-addons-using-c-and-n-api-node-addon-api-9b3b718a9a7f

https://github.com/nodejs/node-gyp

https://github.com/zeit/pkg

https://github.com/parro-it/screen-info


https://www.splinter.com.au/service-discovery-using-nodejs-and-ssdp-unive/


Keylogger info:

https://securelist.com/keyloggers-implementing-keyloggers-in-windows-part-two/36358/



TLS Server:

https://riptutorial.com/node-js/example/19326/tls-socket--server-and-client