import { EventEmitter } from "events";
import { TLSSocket } from "tls";
import { v1 as uuid } from 'uuid';
import { config, updateConfig, saveConfig } from "./config";
import { getScreenInformation, Screen } from "./screen";
import * as _ from 'lodash';

/**
 * Monitors the current input state and controls the current context
 * of the system in terms of active IO devices, which devices we are sending input to, etc
 */

export interface Profile {
    id: string;
    clientId: string;
    name: string;

    type: 'server' | 'client';

    mouseInput?: boolean;
    mouseOutput?: boolean;
    keyboardInput?: boolean;
    keyboardOutput?: boolean;
    audioInput?: boolean;
    audioOutput?: boolean;
    displayInput?: boolean;
    displayOutput?: boolean;

    screens: Screen[];
}

export interface ClientProfile extends Profile {
    type: 'client';
    server: string;
    address?: string;
    port?: number;

    // x and y relative to server primary display
    x?: number;
    
    // y relative to server primary display
    y?: number;

    /**
     * Connection information. Used by server Not saved not sent
     */
    conn?: {
        connected?: boolean;
        lastHeartbeat?: number;
        lastAddress?: string;

        // If set, the addresses this client is allowed to connect from
        addressWhitelist?: string[];

        socket: TLSSocket;
    }
}

export interface ServerProfile extends Profile {
    type: 'server';

    // An array of clients, including the server
    clients: Profile[];


    /**
     * The current state of the server. It is not saved to file.
     */
    state?: {

        mouseAndKeyboardProfile?: string;
        clipboard?: string;

        mouseX?: number;
        mouseY?: number;
    }


    // TODO: place to store files for sending and retrieving?

}

/**
 * An event emitter that outputs whenever the profile changes.
 * 
 * Events:
 * change: the profile was changed to a different profile
 * update: the current profile had properties that were changed
 */
export const profileEmitter = new EventEmitter();


export function isServerProfile(profile: Profile): profile is ServerProfile {
    return profile && profile.type === 'server';
}

export function isClientProfile(profile: Profile): profile is ClientProfile {
    return profile && profile.type === 'client';
}

export function generateDefaultServerProfile(): ServerProfile {
    const id = uuid();

    return {
        id,
        name: 'Default Server',
        clientId: config.id,
        type: 'server',
        clients: [],
        state: {
            mouseAndKeyboardProfile: id,
        },

        mouseInput: true,
        mouseOutput: true,
        keyboardInput: false,
        keyboardOutput: true,

        audioInput: false,
        audioOutput: false,
        displayInput: false,
        displayOutput: false,
        screens: getScreenInformation(),
    }
}

export function generateDefaultClientProfile(
    address: string,
    port = 42777,
): ClientProfile {
    return {
        id: uuid(),
        name: 'Default Client',
        clientId: config.id,
        type: 'client',
        server: undefined,


        address,
        port,

        mouseInput: false,
        mouseOutput: true,
        keyboardInput: false,
        keyboardOutput: true,

        audioInput: false,
        audioOutput: false,
        displayInput: false,
        displayOutput: false,
        screens: getScreenInformation(),
    }
}

export function stripProfile(profile: Profile) {

    let newProfile = {
        ...profile,
        conn: undefined,
    }

    // if (isClientProfile(profile)) {
    //     delete profile.conn;
    // }
    if (isServerProfile(profile) && profile.clients) {
        for (const client in profile.clients) {
            if (profile.clients[client]) {
                newProfile[client] = {
                    ...profile.clients[client],
                    conn: undefined,
                }
            }
        }
    }

    return newProfile;
    
    
}

export function setCurrentProfile(
    profileId: string,
) {
    const profile = getProfileById(profileId);
    if (!profile) { return; }
    config.currentProfile = profileId;
    profileEmitter.emit('change');
    console.log(`Profile set to ${ profile.name } (${ profileId })`);
}

export function getProfileById(id: string) {
    const profile = config.profiles.find((p) => p.id === id);
    return profile;
}

export function getCurrentProfile() {
    return getProfileById(config.currentProfile);
}

export function addProfile(
    profile: Profile,
) {
    config.profiles.push(profile);
}

/**
 * Sets the current context and emits an event to contextChange
 */
// export function setServerContext(
//     context: ServerContext | string,
// ) {
//     const existingIndex = serverContextList.findIndex((c) => {
//         if (typeof context === 'string') {
//             return c.contextName === context;
//         } else {
//             return c.contextName === context.contextName;
//         }
//     });

//     if (existingIndex >= 0 && typeof context === 'object') {
//         serverContextList[existingIndex] = context;
//         context = context;
//     } else if (typeof context === 'object') {
//         serverContextList.push(context);
//         context = context;
//     } else if (existingIndex >= 0 && typeof context === 'string') {
//         context = serverContextList[existingIndex];
//     } else if (typeof context === 'string') {
//         throw new Error(`No context with name ${ context } exists.`);
//     }

//     if (!context || typeof context !== 'object' || !context.contextName) {
//         throw new Error(`Could not evaluate context when setting`);
//     }

//     serverContext = context;

//     profileEmitter.emit('change');

// }

export async function saveContexts() {

    // TODO: save context in config
}


export function onClientInit(
    client: Profile,
) {
    const profile = getCurrentProfile();

    if (
        !client.id || !client.clientId ||
        !client.screens || client.screens.length === 0 ||
        !isClientProfile(client)
    ) { return; }

    if (!isServerProfile(profile)) {
        throw new Error(`Can't init client because server context is not set`);
    }



    // client.server = false;
    // client.client = true;

    const index = profile.clients.findIndex((cc) => cc.id == client.id);
    if (index >= 0) {
        // TODO: this may cause issues with the screens array when
        // adding and removing screens
        // TODO: mergewidth for screens
        _.merge(profile.clients[index], client)
    } else {
        profile.clients.push(client);
    }

    profileEmitter.emit('update');
    
    return client;
}




