import { EventEmitter } from "events";
import { config } from "./config";
import { isKeyResult, isMouseResult, KeyListenResult, listenInput, MouseListenResult, cleanupInput } from "./screen";

export interface KeyState {
    [ keyCode: number ]: boolean;
}

export interface MouseState {
    x?: number;
    y?: number;
    left?: boolean;
    right?: boolean;
    middle?: boolean;
    x1?: boolean;
    x2?: boolean;
}

let lastKeyState: KeyState = {};
let keyState: KeyState = {};
let mouseState: MouseState = {};
let lastX: number;
let lastY: number;

export const onLocalInput = new EventEmitter();
export const onCollatedLocalInput = new EventEmitter();

let collatedInputInterval: NodeJS.Timeout;

function processMouseResult(result: MouseListenResult) {
    if (result.type === 'm.') {
        // unknown type
        console.log(result);
        return;
    }

    // console.log(result);
    result.dx = lastX !== undefined ? result.x - lastX : 0;
    result.dy = lastY !== undefined ? result.y - lastY : 0;

    lastX = result.x;
    lastY = result.y;

    onLocalInput.emit('input', result);
}

function processKeyResult(result: KeyListenResult) {
    if (result.type === 'k.') {
        // unknown type
        console.log(result);
        return;
    }

    if (result.type === 'kd') {
        keyState[result.keyCode] = true;
    } else {
        keyState[result.keyCode] = false;
    }

    onLocalInput.emit('input', result);
}

export function startInputListener() {
    listenInput((result) => {
        let type = '';
        try {
            if (isMouseResult(result)) {
                type = 'mouse ';
                processMouseResult(result);
            } else if (isKeyResult(result)) {
                type = 'keyboard ';
                processKeyResult(result);
            }
        } catch (e) {
            console.error(`Could not process ${ type }result`);
            console.error(e);
            console.error(result);
        }
    });

    collatedInputInterval = setInterval(() => {
        // TODO: get collated input
        onCollatedLocalInput.emit('input');
        
    }, config.collatedInputInterval);
}

export function stopInputListener() {
    if (collatedInputInterval) {
        clearInterval(collatedInputInterval);
        collatedInputInterval = undefined;
    }
    cleanupInput();
}

export enum VIRTUAL_KEYS {
    LSHIFT = 160,
    RSHIFT = 161,
    LCTRL = 162,
    RCTRL = 163,
    RALT = 165,
    LALT = 164,
    f = 70,
    r = 82,
    o = 79,
    g = 71,
}
