const inputAddon = require('../build/Release/inputaddon.node');


export interface Screen {
    name: string;
    primary: boolean;
    top: number;
    right: number;
    bottom: number;
    left: number;
}

export interface ListenResult {
    type: string;
}

export interface KeyListenResult extends ListenResult {
    type: KeyType;
    keyCode: number;

    // If windows, the ID of the event that happened.
    winid?: number;
}

export interface MouseListenResult extends ListenResult {
    type: MouseType,
    x?: number;
    y?: number;
    dx?: number;
    dy?: number;

    // The x button that was pressed
    mbxy?: number;
    
    // Mouse wheel delta
    mwd?: number;
}

/**
 * k = key down and key up
 * ku = key up
 * kd = key down
 * k. = unknown key action
 */
type KeyType = 'k' | 'ku' | 'kd' | 'k.';

type MouseType = 
    'm.'   | // unknown/invalid mouse type
    'mm'   | // mouse move
    'mma'  | // mouse move, absolute
    'mbrd' | // mouse button right down
    'mbru' | // mouse button right up
    'mbld' | // mouse button left down
    'mblu' | // mouse button left up
    'mbmd' | // mouse button middle down
    'mbmu' | // mouse button middle up
    'mbxd' | // mouse button x down
    'mbxu' | // mouse button x up
    'mbnd' | // mouse button ncx down
    'mbnu' | // mouse button ncx up
    'mw'   | // mouse wheel
    'mwh';   // mouse wheel (horizontal)

export function isMouseResult(result: ListenResult): result is MouseListenResult {
    return result.type.startsWith('m');
}

export function isKeyResult(result: ListenResult): result is KeyListenResult {
    return result.type.startsWith('k');
}

export function getScreenInformation() {
    const screens = inputAddon.Screen.getScreenInformation() as Screen[];

    for (const screen of screens) {
        screen.name = screen.name.replace(/([^0-9A-Za-z])/g, '');
    }

    return screens;
}

export function sendMouseInput(input: MouseListenResult) {
    sendInput(input);
}

export function sendKeyboardInput(input: KeyListenResult) {
    if (input.type === 'k') {
        sendInput({
            type: 'kd',
            ...input,
        });
        sendInput({
            type: 'ku',
            ...input,
        });
    } else {
        sendInput(input);
    }
}

export function sendInput(input: MouseListenResult | KeyListenResult) {
    inputAddon.Screen.sendInput(input);
}

export function listenInput(onInput: (result: KeyListenResult | MouseListenResult) => void): void {
    inputAddon.Screen.listenInput(onInput);
}

export function cleanupInput(): void {
    inputAddon.Screen.cleanupInput();
}

export function blockInput(blockInput: boolean): void {
    inputAddon.Screen.blockInput(blockInput);
}

export interface CaptureResult {
    bytes: Buffer;
}

export function captureWindow(cb: (res: CaptureResult) => void) {
    inputAddon.Screen.captureWindow(cb);
}
