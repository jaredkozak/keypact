import { CaptureResult } from "./screen";
import { join } from "path";
import { app, BrowserWindow } from 'electron'

// https://github.com/sudhakar3697/electron-alternatives

export function onCapture(res: CaptureResult) {
    console.log(res.bytes.toString('base64'));
}

let win: BrowserWindow;

export function startExternalWindowingClient() {
    app.whenReady().then(() => {
        
    // Create the browser window.
        let win = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
            nodeIntegration: true
            }
        })
    
        // and load the index.html of the app.
        win.loadFile('http/stream.html').catch(console.error);
    })
}

export function stopExternalWindowingClient() {

}

export function restartExternalWindowingClient() {
    stopExternalWindowingClient();
    startExternalWindowingClient();
}