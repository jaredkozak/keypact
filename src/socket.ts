import { TLSSocket } from "tls";

export const DEFAULT_PORT = 42777;

export interface TLSMessage {
    type: MessageType;
    data?: any;
}

export enum MessageType {
    input = 'input',
    heartbeat = 'heartbeat',
    client_init = 'client_init',
    server_init = 'server_init',
}

export async function writeMessageToSocket(
    socket: TLSSocket,
    type: MessageType,
    data: any,
) {

    const msg: TLSMessage = {
        type,
        data,
    };

    return new Promise((resolve, reject) => socket.write(JSON.stringify(msg), (err) => {
        if (err) {
            reject(err);
        } else {
            resolve(msg);
        }
    }));
}


/**
 * A non-promise based version of writeMessageToSocket that logs on any error
 * @param socket 
 * @param type 
 * @param data 
 */
export function writeMessageToSocketSafe(
    socket: TLSSocket,
    type: MessageType,
    data: any,
) {
    return writeMessageToSocket(socket, type, data).catch((e) => console.error(e));
}

export function processMessage(
    raw: Buffer,
) {
    let msg: TLSMessage;
    try {
        msg = JSON.parse(raw.toString());
    } catch (e) {
        console.error(e);
        return;
    }

    return msg;
}
