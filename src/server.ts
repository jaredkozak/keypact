import * as fs from 'fs';
import * as tls from 'tls';
import { config, saveConfig } from './config';
import { onLocalInput } from './input';
import { getCurrentProfile, isServerProfile, onClientInit, Profile, profileEmitter, stripProfile } from './profile';
import { ListenResult } from './screen';
import { MessageType, processMessage, writeMessageToSocketSafe } from './socket';

let server: tls.Server;

export async function startServer() {
    const profile = getCurrentProfile();
    if (server || !isServerProfile(profile)) { return; }
    // TODO: generate certificates
    console.log('Starting TLS server');

    const options: tls.TlsOptions = {
        key: fs.readFileSync(config.certificate.keyPath),
        cert: fs.readFileSync(config.certificate.certPath),
    };

    server = tls.createServer(options, handleSocket);

    return new Promise((resolve, reject) => {
        try {
            server.listen(config.tlsServerPort, config.tlsServerHost, 0, resolve);
        } catch(e) {
            reject(e);
        }
    });
}

export async function stopServer() {
    if (!server) { return; }
    console.log('Stopping TLS server');
    return new Promise((resolve, reject) => server.close((err) => {
        if (err) { reject(err); }
        server = undefined;
        resolve();
    }));
}

export async function restartServer() {
    await stopServer();
    await startServer();
}

export function handleSocket(socket: tls.TLSSocket) {
    let clientName = socket.remoteAddress;
    let clientContext: Profile;
    const serverProfile = getCurrentProfile();

    console.log(`Client connected: ${ clientName }`);

    // Send a friendly message
    // blockInput(true);

    // Print the data that we received
    socket.on('data', function(raw) {
        const msg = processMessage(raw);
        if (!msg) { return; }

        if (msg.type === MessageType.client_init) {
            clientContext = onClientInit(msg.data);
            if (!clientContext) {
                console.error(`Invalid client context: ${ JSON.stringify(msg.data) }`);
                end();
                return;
            }

            onInit();
        }

    });

    socket.on('error', function(err) {
        if (err.code === 'ECONNRESET') {
            end();
        } else {
            console.log(err);
        }
    });

    // Let us know when the transmission is over
    socket.on('end', function() {
        end();
    });
    


    function end() {
        onLocalInput.removeListener('input', onInput);
        profileEmitter.removeListener('change', onContextChanged);
        console.log(`Client ended: ${ clientName }`);
        socket.destroy();
    }

    function onInit() {

        // register input listener to send client input information
        onLocalInput.addListener('input', onInput);

        // Let the clients know about any updates to the servers current context
        profileEmitter.addListener('change', onContextChanged)

        // send client information about the server context
        writeMessageToSocketSafe(socket, MessageType.server_init, stripProfile(serverProfile));

        // TODO register heartbeat

        saveConfig().catch(console.error);
    }

    function onInput(input: ListenResult) {
        const profile = getCurrentProfile();
        if (!profile || !clientContext || !isServerProfile(profile)) { return; }
        if (profile.state.mouseAndKeyboardProfile !== clientContext.id) { return; }

        // console.log(input);
        writeMessageToSocketSafe(socket, MessageType.input, input);
    }

    function onContextChanged() {

        writeMessageToSocketSafe(socket, MessageType.server_init, stripProfile(serverProfile));
    }
}

