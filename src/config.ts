import { EventEmitter } from 'events';
import { promises as fs, unwatchFile, watch } from 'fs';
import * as _ from 'lodash';
import { v1 as uuid } from 'uuid';
import { argv } from 'yargs';
import { Profile, ServerProfile, ClientProfile } from './profile';

// The config that is saved in the keypact-config.json file
export let savedConfig: KeypactConfig;

// The combination of keypact-config.json and command line arguments.
export let config: KeypactConfig;

export const onConfigChange = new EventEmitter();

// Generated from generateDefaultKeypactConfig
export type KeypactConfig = ReturnType<typeof generateDefaultKeypactConfig>;

let reloading = false;

export function getConfigPath() {

    if (argv.config) {
        return argv.config;
    }

    if (process.env.KEYPACT_CONFIG_PATH) {
        return process.env.KEYPACT_CONFIG_PATH;
    }

    return 'config/keypact-config.json';
}

export function generateDefaultKeypactConfig() {

    return {
        id: uuid(),
        tlsServerHost: '0.0.0.0',
        tlsServerPort: 42777,
        // tlsServer: true,

        certificate: {
            keyPath: 'config/test-key.pem',
            certPath: 'config/test-cert.pem',
        },

        // client: false,
        // clientServerAddress: '127.0.0.1',
        // clientServerPort: 42777,
        clientReconnectInterval: 5000,
        collatedInputInterval: 5,
        currentProfile: null as string,
        profiles: [] as ( Profile | ServerProfile | ClientProfile )[],
    };
}

/**
 * Calls getConfig and monitors the config file for any changes.
 * 
 * If there is a change that has been made to the config file,
 * onConfigChange will be called with 'change' event
 */
export async function monitorConfig() {
    await loadConfig();

    watch(getConfigPath(), {
        // persistent: false,
    }, async (eventType, filename) => {
        if (eventType !== 'change' || reloading) { return; }
        reloading = true;
        console.log('Keypact configuration updated');

        // reload configuration from file
        const newConfig = await loadConfig(false).catch((e) => {
            console.error('Error reading new config');
            console.error(e);
        }).then((r) => r);

        if (newConfig) {
            onConfigChange.emit('change');
        }
        reloading = false;
    });

    // console.log(config);

    return savedConfig;
}

export function stopMonitoringConfig() {
    unwatchFile(getConfigPath());
}

export function getConfig() {
    if (!savedConfig) { throw new Error('Config not set'); }

    return savedConfig;
}

/**
 * Reads the configuration from the config json file and
 * sets the config variable to its output.
 * 
 * If the configuration file does not exist, it creates it with
 * the result from generateDefaultKeypactConfig
 * 
 * @returns config variable
 * 
 */
export async function loadConfig(update = true): Promise<KeypactConfig> {
    
    try {
        savedConfig = JSON.parse( await fs.readFile(getConfigPath(), 'utf8') );
    } catch (e) {
        if (e.code === 'ENOENT') {
            savedConfig = generateDefaultKeypactConfig();
        } else {
            throw e;
        }
    }

    if (update) {
        await updateConfig();
    }

    config = processOverridenConfig();

    return savedConfig;
}

export async function setConfig(newConfig) {
    await fs.writeFile(getConfigPath(), JSON.stringify(newConfig, null, 2));
    savedConfig = newConfig;
    return newConfig;
}

/**
 * Saves the current config state
 */
export async function saveConfig() {
    reloading = true;
    await setConfig(config);
    reloading = false;

}

export async function updateConfig() {
    // place renames and removals here.

    await setConfig({
        ...generateDefaultKeypactConfig(),
        ...savedConfig,
    });
}

export function processOverridenConfig() {
    config = _.cloneDeep(savedConfig);
    config = _.merge(argv, config);

    return config;
}
