import { restartClient, startClient, stopClient } from "./client";
import { config, getConfigPath, monitorConfig, onConfigChange, saveConfig, stopMonitoringConfig } from "./config";
import { restartContextWatcher, startContextWatcher } from "./context";
import { startInputListener, stopInputListener } from "./input";
import { addProfile, generateDefaultServerProfile, getCurrentProfile, profileEmitter } from "./profile";
import { restartServer, startServer, stopServer } from "./server";
import { startExternalWindowingClient, restartExternalWindowingClient, stopExternalWindowingClient } from "./externalWindowing";


async function start() {
    // const screens = getScreenInformation();
    // console.log(screens);

    // LOAD CONFIGURATION AND START MONITORING
    await monitorConfig();
    console.log(`Retrieved config from ${ getConfigPath() }`);

    if (config.profiles.length === 0) {

        const serverProfile = generateDefaultServerProfile();
        addProfile(serverProfile);
        config.currentProfile = serverProfile.id;
        await saveConfig();
    }

    let profile = getCurrentProfile();
    if (!profile) {
        // TODO: don't exit without a profile...
        console.log(`Profile not set, exiting`);
        await stop();
        return;
    }

    await startServer();
    await startClient();

    startContextWatcher();
    startInputListener();
    startExternalWindowingClient();

    onConfigChange.addListener('change', restart);
    profileEmitter.addListener('change', restart);
}

function restart() {
    stopInputListener();
    startInputListener();

    restartContextWatcher();
    restartExternalWindowingClient();

    restartServer().catch((e) => {
        console.error('Could not restart server');
        console.error(e);
        // TODO: consider exiting here
    });
    restartClient().catch((e) => {
        console.error('Could not restart client');
        console.error(e);
        // TODO: consider exiting here
    });
}

async function stop() {

    console.log('Stopping keypact...');
    stopInputListener();
    stopExternalWindowingClient();
    onConfigChange.removeAllListeners();

    stopMonitoringConfig();
    await stopServer();
    await stopClient();
    console.log('Stopped.');
}


start().catch((e) => {
    console.error(`Could not start keypact`);
    console.error(e);
});

// does not work yet
process.on('SIGINT', async () => {
    await stop();
    process.exit();
});
