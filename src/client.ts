import * as fs from 'fs';
import * as tls from 'tls';
import { config } from "./config";
import { VIRTUAL_KEYS } from "./input";
import { getCurrentProfile, isClientProfile, stripProfile } from './profile';
import { isKeyResult, isMouseResult, ListenResult, sendMouseInput } from "./screen";
import { MessageType, processMessage, writeMessageToSocket } from './socket';

let client: tls.TLSSocket;
let lastConnectAttempt: Date;

export async function startClient() {
    const profile = getCurrentProfile();
    if (client || !isClientProfile(profile) || !profile.address) { return; }
    console.log('Starting client');
    lastConnectAttempt = new Date();

    const options: tls.ConnectionOptions = {
        ca: fs.readFileSync(config.certificate.certPath, 'utf-8'),
        rejectUnauthorized: true,
        requestCert: true,
        
    };

    client = tls.connect(
        profile.port || 42777,
        profile.address,
        options,
        onConnect,
    );
    
}

export function stopClient() {
    if (!client) { return; }
    client.destroy();
    client = undefined;
}

export async function restartClient() {
    stopClient();
    await startClient();
}

export function reconnect() {
    stopClient();

    let delay = 0;
    if (lastConnectAttempt) {
        const diff = Date.now() - lastConnectAttempt.getTime();
        delay = config.clientReconnectInterval - diff;
    }

    if (delay > 0) {
        console.log(`Reconnecting client in ${ delay / 1000 } seconds...`);
        setTimeout(doReconnect, delay);
    } else {
        console.log(`Reconnecting client...`);
        doReconnect();
    }

    function doReconnect() {
    
        startClient().catch((e) => {
            console.error('Could not restart client');
            console.error(e);
            reconnect();
        });
    }



}

export function onConnect() {

    // Check if the authorization worked
    if (client.authorized) {
        console.log("Connection authorized by a Certificate Authority.");
    } else {
        console.log("Connection not authorized: " + client.authorizationError);
        return;
    }

    // Send a friendly message
    client.on('data', (msgRaw: Buffer) => {
        const msg = processMessage(msgRaw);
        if (!msg) { return; }
        
        if (msg.type === MessageType.input) {
            resolveClientInput(msg.data);
        }
    });

    client.on('error', (err) => {
        if (err.code === 'ECONNRESET') {
            reconnect();
        } else {
            console.error(err);
        }
    });

    client.on('end', function() {
        reconnect();
    });

    writeMessageToSocket(
        client, MessageType.client_init,
        stripProfile(getCurrentProfile()),
    ).catch((e) => {
        reconnect();
    });

    // TODO: heartbeat

    async function write(buffer: string) {
        return new Promise((resolve, reject) => {
            client.write(buffer, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(true);
                }
            });
        });
    }
}

let lastX: number;
let lastY: number;
let logging = false;

function resolveClientInput(input: ListenResult) {
    // console.log(input);
    if (isKeyResult(input)) {
        
        if (input.type === 'ku' && input.keyCode === VIRTUAL_KEYS.RSHIFT) {
            logging = !logging;
        }

    }

    if (isMouseResult(input) && input.type === 'mm') {

        
        if (lastX !== undefined && lastY !== undefined) {
            const dx = lastX - input.x;
            const dy = lastY - input.y;
            if (logging) {
                console.log(dx, dy);
            }
            sendMouseInput({
                type: 'mm',
                x: -dx,
                y: -dy,

            });
        }

        lastX = input.x;
        lastY = input.y;

    }
}
