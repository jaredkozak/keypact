import { onLocalInput, VIRTUAL_KEYS } from "./input";
import { getCurrentProfile, isClientProfile, isServerProfile, profileEmitter } from "./profile";
import { captureWindow, isKeyResult, isMouseResult, ListenResult } from "./screen";
import { onCapture } from "./externalWindowing";

export function startContextWatcher() {
    const profile = getCurrentProfile();

    onLocalInput.addListener('input', onInput);

    if (isServerProfile(profile)) {
    }

    if (isClientProfile(profile)) {
    }
    // const cc = generateClientContext();

    // profileEmitter.addListener('change', () => {
    //     console.log(serverContext);
    // });

    // if () {
    //     // TODO: load from config, use contextName from variable
    //     // or generate a new one if it does not exist
    //     // setServerContext({
    //     //     contextName: 'Default',
    //     //     server: cc.id,
    //     //     mouseAndKeyboardContext: cc.id,
    //     //     clients: [
    //     //         cc,
    //     //     ],
    //     // });

    // }
    

}

function onInput(input: ListenResult) {
    const profile = getCurrentProfile();
    if (!profile) { return; }
    if (!isServerProfile(profile)) {
        // TODO: send input to server
        return;
    }
    
    if (profile.mouseInput && isMouseResult(input)) {
        const chewie = '32058ba0-619f-11ea-9209-136c9ab016e2';
        if (input.x >= 5360 && profile.state.mouseAndKeyboardProfile !== chewie) {
            profile.state.mouseAndKeyboardProfile = chewie;
            profileEmitter.emit('update');
        }
    }

    if (profile.keyboardInput && isKeyResult(input)) {

        // if (input.keyCode === 163 && input.type === 'ku') {
        //     blockInput(true);
        //     const interval = setInterval(() => {
        //         sendMouseInput({
        //             type: 'mm',
        //             x: 0,
        //             y: -3,
        //         })
        //         console.log('move up');
        //     }, 3);

        //     setTimeout(() => {
        //         clearInterval(interval);
        //         blockInput(false);
        //     }, 100);
        // }

        if (input.keyCode === VIRTUAL_KEYS.RCTRL && input.type === 'ku') {
            captureWindow(onCapture);
        }
    }
    // console.log(input);
}

export function stopContextWatcher() {
    onLocalInput.removeListener('input', onInput)
}

export function restartContextWatcher() {
    stopContextWatcher();
    startContextWatcher();
}

function clientProcessing() {

}

function serverProcessing() {

}
