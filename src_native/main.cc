#include <napi.h>
#include "common/screenWrapper.h"

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {

    return ScreenWrapper::Init(env, exports);
}

NODE_API_MODULE(NODE_GYP_MODULE_NAME, InitAll)