#include "screen.h"
#include <napi.h>

using namespace ScreenInfo;

Screen::Screen(std::string name, bool primary, int top, int right, int bottom, int left) {
  this->name = name;
  this->primary = primary;
  this->top = top;
  this->right = right;
  this->bottom = bottom;
  this->left = left;
}
