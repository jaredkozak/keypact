#ifndef SCREENWRAPPER
#define SCREENWRAPPER

#include <napi.h>
#include "screen.h"

class ScreenWrapper : public Napi::ObjectWrap<ScreenWrapper> {
    public:
    static Napi::Object Init(Napi::Env env, Napi::Object exports); //Init function for setting the export key to JS
    ScreenWrapper(const Napi::CallbackInfo& info); //Constructor to initialise

    private:
    static Napi::FunctionReference constructor; //reference to store the class definition that needs to be exported to JS

    // static Napi::Value main(const Napi::CallbackInfo& info);
    static Napi::Value getScreenInformation(const Napi::CallbackInfo& info);
    static void blockInput(const Napi::CallbackInfo& info);
    static void listenInput(const Napi::CallbackInfo& info);
    static void cleanupInput(const Napi::CallbackInfo& info);
    static void sendInput(const Napi::CallbackInfo& info);
    static void captureWindow(const Napi::CallbackInfo& info);
    static void stopCapturingWindow(const Napi::CallbackInfo& info);

    static Napi::Object convertScreenToObject(Napi::Env& env, ScreenInfo::Screen* screen);

};

#endif