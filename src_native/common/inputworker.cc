#include "inputworker.h"

InputWorker::InputWorker(Function &callback) : AsyncWorker(callback) {

}

void InputWorker::Execute()
{
    // for (size_t i = 0; i < dataLength; i++)
    // {
    //     uint8_t value = *(dataPtr + i);
    //     *(dataPtr + i) = value * 2;
    // }
}

void InputWorker::OnOK()
{
    HandleScope scope(Env());
    Callback().Call({
        Env().Null(),
    });

    // dataRef.Unref();
}