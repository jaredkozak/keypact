#define NAPI_VERSION 4

#include "screenWrapper.h"
#include "inputworker.h"
#include <iostream>
#include <napi.h>

Napi::FunctionReference ScreenWrapper::constructor;

Napi::Object ScreenWrapper::Init(Napi::Env env, Napi::Object exports) {
    Napi::HandleScope scope(env);

    Napi::Function func = DefineClass(env, "Screen", {
        StaticMethod("getScreenInformation", &ScreenWrapper::getScreenInformation),
        StaticMethod("listenInput", &ScreenWrapper::listenInput),
        StaticMethod("cleanupInput", &ScreenWrapper::cleanupInput),
        StaticMethod("blockInput", &ScreenWrapper::blockInput),
        StaticMethod("sendInput", &ScreenWrapper::sendInput),
        StaticMethod("captureWindow", &ScreenWrapper::captureWindow),
        StaticMethod("stopCapturingWindow", &ScreenWrapper::stopCapturingWindow)

    });

    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();

    exports.Set("Screen", func);
    return exports;
}

ScreenWrapper::ScreenWrapper(const Napi::CallbackInfo& info) : Napi::ObjectWrap<ScreenWrapper>(info)  {}

void ScreenWrapper::listenInput(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    if (info.Length() < 1 || !info[0].IsFunction()) {
        Napi::TypeError::New(env, "Callback expected").ThrowAsJavaScriptException();
    }

    Napi::Function callback = info[0].As<Napi::Function>();


    ScreenInfo::Screen::listenInput(&callback, &env);


}
void ScreenWrapper::cleanupInput(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    ScreenInfo::Screen::cleanupInput();


}

void ScreenWrapper::blockInput(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    if (info.Length() < 1 || !info[0].IsBoolean()) {
        Napi::TypeError::New(env, "Boolean expected").ThrowAsJavaScriptException();
    } 

    ScreenInfo::Screen::blockInput(info[0].As<Napi::Boolean>().Value());
}

Napi::Value ScreenWrapper::getScreenInformation(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    std::vector<ScreenInfo::Screen> screens = ScreenInfo::Screen::getScreenInformation();
    int nScreens = screens.size();

    // std::cout << nScreens;
    // std::cout << "\n";

    Napi::Array screenObjects = Napi::Array::New(env, nScreens);
    for (int i = 0; i < nScreens; i++) {
        screenObjects[i] = ScreenWrapper::convertScreenToObject(env, &screens[i]);
    }

    return screenObjects;
}

void ScreenWrapper::sendInput(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);


    if (info.Length() < 1 || !info[0].IsObject()) {
        Napi::TypeError::New(env, "Input object expected").ThrowAsJavaScriptException();
    }

    Napi::Object obj = info[0].As<Napi::Object>();
    ScreenInfo::COMMON_INPUT input;

    if (obj.Get("type").IsUndefined()) {
        Napi::TypeError::New(env, "Type must be defined").ThrowAsJavaScriptException();
    }

    input.type = (std::string) obj.Get("type").ToString();

    input.x = (int) obj.Get("x").ToNumber();
    input.y = (int) obj.Get("y").ToNumber();
    input.mbxy = (int) obj.Get("mbxy").ToNumber();
    input.mwd = (int) obj.Get("mwd").ToNumber();
    input.keyCode = (int) obj.Get("keyCode").ToNumber();


    ScreenInfo::Screen::sendInput(&input);

}

void ScreenWrapper::captureWindow(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    if (info.Length() < 1 || !info[0].IsFunction()) {
        Napi::TypeError::New(env, "Callback expected").ThrowAsJavaScriptException();
    }

    Napi::Function callback = info[0].As<Napi::Function>();


    ScreenInfo::Screen::captureWindow(&callback, &env);
}
void ScreenWrapper::stopCapturingWindow(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();
    Napi::HandleScope scope(env);

    ScreenInfo::Screen::stopCapturingWindow();
}

Napi::Object ScreenWrapper::convertScreenToObject(Napi::Env& env, ScreenInfo::Screen* screen) {

    Napi::Object obj = Napi::Object::New(env);
    obj.Set("name", screen->name);
    obj.Set("primary", screen->primary);
    obj.Set("top", screen->top);
    obj.Set("left", screen->left);
    obj.Set("bottom", screen->bottom);
    obj.Set("right", screen->right);
    // obj.Set("colorDepth", screen->getColorDepth());

    return obj;
}
