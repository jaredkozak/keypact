#ifndef SCREEN
#define SCREEN

#include <napi.h>
#include "inputworker.h"

namespace ScreenInfo {


  // input common to all operator systems, defined in screenWrapper.cc
  struct COMMON_INPUT {
    std::string type;

    /**
     * MOUSE INPUT
     */

    int x;
    int y;

    // the mouse button that was pressed
    int mbxy;

    // mouse wheel delta
    int mwd;

    /**
     * KEYBOARD INPUT
     */

    int keyCode;
  };

  class Screen {
    private:

    public:
      Screen(
        std::string name,
        bool primary,
        int top,
        int right,
        int bottom,
        int left
      );
      // size_t getWidth();
      // size_t getHeight();
      // size_t getWidthMM();
      // size_t getHeightMM();
      // int getColorDepth();

      std::string name;
      bool primary;
      int top;
      int right;
      int bottom;
      int left;

      static bool inputListening;
      static bool capturing;
      static bool blocking;

      // static Screen main();
      static std::vector<Screen> getScreenInformation();
      static void sendInput(COMMON_INPUT* input);

      // Blocks input and hides cursor if true
      static void blockInput(bool blockInput);
      static bool listenInput(Napi::Function* c, Napi::Env* env);
      static void cleanupInput();

      static bool captureWindow(Napi::Function* c, Napi::Env* env);
      static bool stopCapturingWindow();
  };
}


#endif