#ifndef INPUTWORKER
#define INPUTWORKER

#include <napi.h>

using namespace Napi;

class InputWorker : public AsyncWorker {
    public:
        InputWorker(Function &callback);

        void Execute();

        void OnOK();

};

#endif