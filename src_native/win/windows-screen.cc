#define NAPI_VERSION 4

#include <windows.h>
#include "../common/screen.h"
#include "../common/inputworker.h"
#include <napi.h>
#include <iostream>
#include <thread>

using namespace ScreenInfo;

BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData) {
    std::vector<Screen> *result = (std::vector<Screen> *)dwData;
    //   int bitsPerPixel = GetDeviceCaps(hdcMonitor, BITSPIXEL);
    //   int hSize = GetDeviceCaps(hdcMonitor,HORZSIZE);
    //   int vSize = GetDeviceCaps(hdcMonitor, VERTSIZE);


    if (hdcMonitor) {
        std::string name("DISPLAY");

        MONITORINFOEXA info;
        info.cbSize = sizeof(info);
        if (GetMonitorInfoA(hMonitor, &info))
        {
            result->push_back(Screen(
                info.szDevice,
                info.dwFlags & MONITORINFOF_PRIMARY,
                (int) lprcMonitor->top,
                (int) lprcMonitor->right,
                (int) lprcMonitor->bottom,
                (int) lprcMonitor->left
            ));
        }
        

    }

    return TRUE;
}

struct MouseInput {
    long wParam;
    MSLLHOOKSTRUCT data;
};

struct KeyboardInput {
    long wParam;
    KBDLLHOOKSTRUCT data;
};

HHOOK keyboardHookId = 0;
HHOOK mouseHookId = 0;
bool ScreenInfo::Screen::blocking = false;
bool ScreenInfo::Screen::inputListening = false;
bool ScreenInfo::Screen::capturing = false;

ThreadSafeFunction tsfn;
ThreadSafeFunction capture_tsfn;


void CALLBACK __stdcall keyboardCallback(Napi::Env env, Napi::Function jsCallback, KeyboardInput* data) {
    
    std::string type = "k.";
    Napi::Object obj = Napi::Object::New(env);
    // Napi::Object obj = Napi::Object(env, Napi::Number::New(env, 1));

    switch (data->wParam) {
        case WM_KEYUP:
        case WM_SYSKEYUP:
            type = "ku";
            break;
        case WM_SYSKEYDOWN:
        case WM_KEYDOWN:
            type = "kd";
            break;
    }

    obj.Set("type", type);
    obj.Set("keyCode", data->data.vkCode);
    obj.Set("state", GetKeyState(data->data.vkCode));
    // obj.Set("scanCode", data->data.scanCode);
    // obj.Set("flags", data->data.flags);
    // obj.Set("x", data->data.pt.x);
    // obj.Set("y", data->data.pt.y);
    // obj.Set("mouseData", mouseData);
    // obj.Set("delta", WHEEL_DELTA);
    // obj.Set("flags", data->data.flags);
    // obj.Set("time", data->data.time);
    // obj.Set("extra", data->data.dwExtraInfo);

    jsCallback.Call({ obj });
    
    // delete data;
};

LRESULT CALLBACK rawKeyboardHook(
  int    nCode,
  WPARAM wParam,
  LPARAM lParam
) {
    if (nCode < 0) {
        return CallNextHookEx(keyboardHookId, nCode, wParam, lParam);
    }

    KeyboardInput data;
    data.wParam = wParam;
    data.data = *((KBDLLHOOKSTRUCT*) lParam);

    // never block windows input thread or else
    tsfn.NonBlockingCall(&data, keyboardCallback);


    // block mouse input from carrying on throughout windows input thread
    if (ScreenInfo::Screen::blocking) {
        // Windows API docs:
        // If the hook procedure processed the message, it may return
        // a nonzero value to prevent the system from passing the message
        // to the rest of the hook chain or the target window procedure.
        return 1;
    }



    // continue with input thread execution
    return CallNextHookEx(keyboardHookId, nCode, wParam, lParam);
}

void CALLBACK __stdcall mouseCallback(Napi::Env env, Napi::Function jsCallback, MouseInput* data) {
    
    std::string type = "m.";
    Napi::Object obj = Napi::Object::New(env);
    // Napi::Object obj = Napi::Object(env, Napi::Number::New(env, 1));

    int mouseData = (int) data->data.mouseData;
    switch (data->wParam) {
        case WM_LBUTTONDOWN:
            type = "mbld";
            break;
        case WM_RBUTTONDOWN:
            type = "mbrd";
            break;
        case WM_LBUTTONUP:
            type = "mblu";
            break;
        case WM_RBUTTONUP:
            type = "mbru";
            break;
        case WM_MOUSEMOVE:
            type = "mm";
            break;
        case WM_MBUTTONDOWN:
            type = "mbmd";
            break;
        case WM_MBUTTONUP:
            type = "mbmu";
            break;
        case WM_XBUTTONDOWN:
            type = "mbxd";
            obj.Set("mbxy", mouseData);
            break;
        case WM_XBUTTONUP:
            type = "mbxu";
            obj.Set("mbxy", mouseData);
            break;
        case WM_NCXBUTTONDOWN:
            type = "mbnd";
            break;
        case WM_NCXBUTTONUP:
            type = "mbnu";
            break;
        case WM_MOUSEWHEEL:
        case WM_MOUSEHWHEEL:
            if (data->wParam == WM_MOUSEWHEEL) {
                type = "mw";
            } else {
                type = "mwh";
            }

            if (mouseData > 0) {
                obj.Set("mwd", WHEEL_DELTA);
            } else {
                obj.Set("mwd", -WHEEL_DELTA);
            }
            break;
        default:
            obj.Set("winid", data->wParam);
            break;

    }

    obj.Set("type", type);
    obj.Set("x", data->data.pt.x);
    obj.Set("y", data->data.pt.y);
    // obj.Set("mouseData", mouseData);
    // obj.Set("delta", WHEEL_DELTA);
    // obj.Set("flags", data->data.flags);
    // obj.Set("time", data->data.time);
    // obj.Set("extra", data->data.dwExtraInfo);

    jsCallback.Call({ obj });
    
    // delete data;
};


LRESULT CALLBACK rawMouseHook(
  int    nCode,
  WPARAM wParam,
  LPARAM lParam
) {
    // Always return this if nCode is less than zero, as per windows docs
    if (nCode < 0) {
        return CallNextHookEx(mouseHookId, nCode, wParam, lParam);
    }

    MouseInput data;
    data.wParam = wParam;
    data.data = *((MSLLHOOKSTRUCT*) lParam);

    // block cursor input from carrying on throughout windows input thread
    if (ScreenInfo::Screen::blocking) {
        std::cout << data.data.mouseData;
        // Windows API docs:
        // If the hook procedure processed the message, it may return
        // a nonzero value to prevent the system from passing the message
        // to the rest of the hook chain or the target window procedure.
        return 1;
    }

    // never block windows input thread or else
    tsfn.NonBlockingCall(&data, mouseCallback);


    // continue with input thread execution
    return CallNextHookEx(mouseHookId, nCode, wParam, lParam);
}

std::vector<Screen> ScreenInfo::Screen::getScreenInformation() {

    std::vector<Screen> result;
    const HWND hDesktop = GetDesktopWindow();
    HDC dc = GetDC(hDesktop);
    EnumDisplayMonitors(dc, NULL, MonitorEnumProc, (LPARAM)&result);
    return result;
}

std::thread listenThread;
std::thread captureThread;

// void FinalizerCallback(Napi::Env env, void *finalizeData);

bool ScreenInfo::Screen::listenInput(Napi::Function* cb, Napi::Env* env) {

    if (ScreenInfo::Screen::inputListening) {
        return false;
    }

    ScreenInfo::Screen::inputListening = true;
    tsfn = Napi::ThreadSafeFunction::New(
        *env,
        *cb,
        "InputCallbackFunction",
        0,
        1
    );

    listenThread = std::thread([] {
        keyboardHookId = SetWindowsHookExA(WH_KEYBOARD_LL, rawKeyboardHook, NULL, 0);
        mouseHookId = SetWindowsHookExA(WH_MOUSE_LL, rawMouseHook, NULL, 0);
        MSG msg;
        
        while (GetMessage(&msg, NULL, 0, 0) && Screen::inputListening) {}

        UnhookWindowsHookEx(keyboardHookId);
        UnhookWindowsHookEx(mouseHookId);
        tsfn.Release();
    });

    return true;
}

void ScreenInfo::Screen::cleanupInput() {
    Screen::inputListening  = false;
    PostThreadMessage(GetThreadId( listenThread.native_handle() ), WM_QUIT, 0, 0);
    listenThread.join();
}

HCURSOR defaultCursor;

void ScreenInfo::Screen::blockInput(bool blockInput) {
    Screen::blocking = blockInput;

}

void ScreenInfo::Screen::sendInput(COMMON_INPUT* input) {

    // This structure will be used to create the keyboard
    // input event.
    INPUT ip;

    const char* type = input->type.c_str();
    int typeLength = strlen(type);
    char hardware = type[0];

    // mouse input
    if (hardware == 'm') {
        ip.type = INPUT_MOUSE;
        // make sure time, extraInfo, and mouseData are zero or you will get crashing
        ip.mi.time = 0;
        ip.mi.dwExtraInfo = 0;
        ip.mi.mouseData = 0;

        char action = input->type[1];
        if (action == 'm') {
            ip.mi.dx = input->x;
            ip.mi.dy = input->y;

            if (typeLength > 2 && type[2] == 'a') {
                // ip.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_VIRTUALDESK;
                // std::cout << ip.mi.dwFlags;
                // std::cout << ip.mi.dx;

                SetCursorPos(input->x, input->y);
                return;
            } else {
                ip.mi.dwFlags = MOUSEEVENTF_MOVE;
            }
        } else if (action == 'b') {
            char button = input->type[2];
            char direction = input->type[3];
            if        (button == 'r' && direction == 'u') { ip.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
            } else if (button == 'r' && direction == 'd') { ip.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
            } else if (button == 'l' && direction == 'u') { ip.mi.dwFlags = MOUSEEVENTF_LEFTUP;
            } else if (button == 'l' && direction == 'd') { ip.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
            } else if (button == 'm' && direction == 'u') { ip.mi.dwFlags = MOUSEEVENTF_MIDDLEUP;
            } else if (button == 'm' && direction == 'd') { ip.mi.dwFlags = MOUSEEVENTF_MIDDLEDOWN;
            } else if (button == 'x' && direction == 'u') { ip.mi.dwFlags = MOUSEEVENTF_XUP;
            } else if (button == 'x' && direction == 'd') { ip.mi.dwFlags = MOUSEEVENTF_XDOWN; }


        }

    // keyboard input
    } else if (hardware == 'k') {\
        ip.type = INPUT_KEYBOARD;
        ip.ki.wScan = 0; // hardware scan code for key
        ip.ki.time = 0;
        ip.ki.dwExtraInfo = 0;
        ip.ki.wVk = input->keyCode;
        ip.ki.dwFlags = 0;

        char direction = input->type[1];
        if (direction == 'u') {
            ip.ki.dwFlags = KEYEVENTF_KEYUP;
        }

    }

    SendInput(1, &ip, sizeof(ip));

}


struct BITMAP_DATA {
    BITMAPFILEHEADER* fileHeader;
    BITMAPINFOHEADER* infoHeader; 
    char* bytes;
    DWORD byteLength;
};

void CALLBACK __stdcall onCaptureCallback(Napi::Env env, Napi::Function jsCallback, BITMAP_DATA* bitmap) {
    Napi::Object obj = Napi::Object::New(env);

    // obj.Set("fileHeader", &bitmap->fileHeader);
    // obj.Set("infoHeader", &bitmap->infoHeader);
    obj.Set( "bytes", Napi::ArrayBuffer::New(env, bitmap->bytes, bitmap->byteLength) );

    jsCallback.Call({ obj });
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_SIZE: {
            int width = LOWORD(lParam);  // Macro to get the low-order word.
            int height = HIWORD(lParam); // Macro to get the high-order word.

            // Respond to the message:
            OnSize(hwnd, (UINT)wParam, width, height);
        }
        break;
        
        case WM_PAINT: {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);

            // All painting occurs here, between BeginPaint and EndPaint.

            FillRect(hdc, &ps.rcPaint, (HBRUSH) (COLOR_WINDOW + 1));

            EndPaint(hwnd, &ps);
        }
        return 0;
        case WM_CLOSE: if (MessageBox(hwnd, L"Really quit?", L"My application", MB_OKCANCEL) == IDOK) {
            DestroyWindow(hwnd);
        }
        return 0;
    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void OnSize(HWND hwnd, UINT flag, int width, int height)
{
    // Handle resizing
}


bool ScreenInfo::Screen::initCapturedWindow(Napi::Function* cb, Napi::Env* env) {
    // we can probably put this all in a new thread.

    const wchar_t CLASS_NAME[]  = L"Sample Window Class";
    WNDCLASS wc = { };

    wc.lpfnWndProc   = WindowProc;
    wc.hInstance     = hInstance;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    HWND hwnd = CreateWindowEx(
        0,                              // Optional window styles.
        CLASS_NAME,                     // Window class
        L"Learn to Program Windows",    // Window text
        WS_OVERLAPPEDWINDOW,            // Window style

        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

        NULL,       // Parent window    
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
    );

    if (hwnd == NULL) {
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);
}


/**
 * Resources:
 * https://stackoverflow.com/questions/10445617/c-how-to-screen-capture-except-for-some-windows
 * https://stackoverflow.com/questions/18167272/how-do-i-perform-bitblt-to-copy-a-screen-region-that-contains-windows-media-pl
 * https://github.com/NickNaso/addon-stream/blob/master/passthrough/src/passthrough.ccH
 */
bool ScreenInfo::Screen::captureWindow(Napi::Function* cb, Napi::Env* env) {
    // if there is not already a capture window thread, create it
    if (!ScreenInfo::Screen::capturing) {

        capture_tsfn = Napi::ThreadSafeFunction::New(
            *env,
            *cb,
            "VideoCapturing",
            0,
            1
        );
        ScreenInfo::Screen::capturing = true;

        captureThread = std::thread([] {
            while (ScreenInfo::Screen::capturing) {
                // TODO: framerate
                // TODO: Loop through each window

                HDC hdc = ::GetDC(NULL); // get the desktop device context
                HDC hDest = CreateCompatibleDC(hdc); // create a device context to use

                // set the height and width of the region
                int height = 800; // TODO: get width/height of window
                int width = 600;

                // create a bitmap
                HBITMAP hBitmap = CreateCompatibleBitmap(hdc, width, height);

                // use the previously created device context with the bitmap
                SelectObject(hDest, hBitmap);

                // copy from the desktop device context (x=100,y=100) to the bitmap device context
                BitBlt(hDest, 0, 0, width, height , hdc, 100, 100, SRCCOPY | CAPTUREBLT);
                // BitBlt(hDest, 0, 0, width, height , hdc, 100, 100, SRCCOPY);

                BITMAP bitmap;
                GetObject(hBitmap, sizeof(BITMAP), &bitmap);

                // TODO: stream w/FFMPEG?

                // WRITE TO FILE

                BITMAPFILEHEADER   bmfHeader;    
                BITMAPINFOHEADER   bi;
                
                bi.biSize = sizeof(BITMAPINFOHEADER);    
                bi.biWidth = bitmap.bmWidth;    
                bi.biHeight = bitmap.bmHeight;  
                bi.biPlanes = 1;    
                bi.biBitCount = 32;    
                bi.biCompression = BI_RGB;    
                bi.biSizeImage = 0;  
                bi.biXPelsPerMeter = 0;    
                bi.biYPelsPerMeter = 0;    
                bi.biClrUsed = 0;    
                bi.biClrImportant = 0;

                DWORD dwBmpSize = ((bitmap.bmWidth * bi.biBitCount + 31) / 32) * 4 * bitmap.bmHeight;

                // Starting with 32-bit Windows, GlobalAlloc and LocalAlloc are implemented as wrapper functions that 
                // call HeapAlloc using a handle to the process's default heap. Therefore, GlobalAlloc and LocalAlloc 
                // have greater overhead than HeapAlloc.
                HANDLE hDIB = GlobalAlloc(GHND,dwBmpSize); 
                char *lpbitmap = (char *)GlobalLock(hDIB);    

                // Gets the "bits" from the bitmap and copies them into a buffer 
                // which is pointed to by lpbitmap.
                GetDIBits(hdc, hBitmap, 0,
                    (UINT)bitmap.bmHeight,
                    lpbitmap,
                    (BITMAPINFO *)&bi, DIB_RGB_COLORS);
    
                // Add the size of the headers to the size of the bitmap to get the total file size
                DWORD dwSizeofDIB = dwBmpSize + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
            
                //Offset to where the actual bitmap bits start.
                bmfHeader.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + (DWORD)sizeof(BITMAPINFOHEADER); 
                
                //Size of the file
                bmfHeader.bfSize = dwSizeofDIB; 
                
                //bfType must always be BM for Bitmaps
                bmfHeader.bfType = 0x4D42; //BM  

                BITMAP_DATA data;
                data.fileHeader = &bmfHeader;
                data.infoHeader = &bi;
                data.bytes = lpbitmap;
                data.byteLength = dwBmpSize;

                capture_tsfn.NonBlockingCall(&data, onCaptureCallback);

                //Unlock and Free the DIB from the heap
                // GlobalUnlock(hDIB);    
                // GlobalFree(hDIB);
                
                // //Clean up
                // DeleteObject(hBitmap);
                // DeleteObject(hDest);
                // ReleaseDC(NULL, hdc);

                // Screen::stopCapturingWindow();
                Screen::capturing = false;
            }
        });
    }

    // TODO: add individual windows to list of desired windows

    return Screen::capturing;
}


bool ScreenInfo::Screen::stopCapturingWindow() {

    // TODO: remove individual windows to list of desired windows
    Screen::capturing = false;

    // Might only belong on individual callbacks.
    capture_tsfn.Release();

    captureThread.join();

    return Screen::capturing;
}
