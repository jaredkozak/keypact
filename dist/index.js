"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("./client");
const config_1 = require("./config");
const context_1 = require("./context");
const input_1 = require("./input");
const profile_1 = require("./profile");
const server_1 = require("./server");
const externalWindowing_1 = require("./externalWindowing");
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        // const screens = getScreenInformation();
        // console.log(screens);
        // LOAD CONFIGURATION AND START MONITORING
        yield config_1.monitorConfig();
        console.log(`Retrieved config from ${config_1.getConfigPath()}`);
        if (config_1.config.profiles.length === 0) {
            const serverProfile = profile_1.generateDefaultServerProfile();
            profile_1.addProfile(serverProfile);
            config_1.config.currentProfile = serverProfile.id;
            yield config_1.saveConfig();
        }
        let profile = profile_1.getCurrentProfile();
        if (!profile) {
            // TODO: don't exit without a profile...
            console.log(`Profile not set, exiting`);
            yield stop();
            return;
        }
        yield server_1.startServer();
        yield client_1.startClient();
        context_1.startContextWatcher();
        input_1.startInputListener();
        externalWindowing_1.startExternalWindowingClient();
        config_1.onConfigChange.addListener('change', restart);
        profile_1.profileEmitter.addListener('change', restart);
    });
}
function restart() {
    input_1.stopInputListener();
    input_1.startInputListener();
    context_1.restartContextWatcher();
    externalWindowing_1.restartExternalWindowingClient();
    server_1.restartServer().catch((e) => {
        console.error('Could not restart server');
        console.error(e);
        // TODO: consider exiting here
    });
    client_1.restartClient().catch((e) => {
        console.error('Could not restart client');
        console.error(e);
        // TODO: consider exiting here
    });
}
function stop() {
    return __awaiter(this, void 0, void 0, function* () {
        console.log('Stopping keypact...');
        input_1.stopInputListener();
        externalWindowing_1.stopExternalWindowingClient();
        config_1.onConfigChange.removeAllListeners();
        config_1.stopMonitoringConfig();
        yield server_1.stopServer();
        yield client_1.stopClient();
        console.log('Stopped.');
    });
}
start().catch((e) => {
    console.error(`Could not start keypact`);
    console.error(e);
});
// does not work yet
process.on('SIGINT', () => __awaiter(void 0, void 0, void 0, function* () {
    yield stop();
    process.exit();
}));