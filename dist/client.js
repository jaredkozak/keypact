"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const tls = require("tls");
const config_1 = require("./config");
const input_1 = require("./input");
const profile_1 = require("./profile");
const screen_1 = require("./screen");
const socket_1 = require("./socket");
let client;
let lastConnectAttempt;
function startClient() {
    return __awaiter(this, void 0, void 0, function* () {
        const profile = profile_1.getCurrentProfile();
        if (client || !profile_1.isClientProfile(profile) || !profile.address) {
            return;
        }
        console.log('Starting client');
        lastConnectAttempt = new Date();
        const options = {
            ca: fs.readFileSync(config_1.config.certificate.certPath, 'utf-8'),
            rejectUnauthorized: true,
            requestCert: true,
        };
        client = tls.connect(profile.port || 42777, profile.address, options, onConnect);
    });
}
exports.startClient = startClient;
function stopClient() {
    if (!client) {
        return;
    }
    client.destroy();
    client = undefined;
}
exports.stopClient = stopClient;
function restartClient() {
    return __awaiter(this, void 0, void 0, function* () {
        stopClient();
        yield startClient();
    });
}
exports.restartClient = restartClient;
function reconnect() {
    stopClient();
    let delay = 0;
    if (lastConnectAttempt) {
        const diff = Date.now() - lastConnectAttempt.getTime();
        delay = config_1.config.clientReconnectInterval - diff;
    }
    if (delay > 0) {
        console.log(`Reconnecting client in ${delay / 1000} seconds...`);
        setTimeout(doReconnect, delay);
    }
    else {
        console.log(`Reconnecting client...`);
        doReconnect();
    }
    function doReconnect() {
        startClient().catch((e) => {
            console.error('Could not restart client');
            console.error(e);
            reconnect();
        });
    }
}
exports.reconnect = reconnect;
function onConnect() {
    // Check if the authorization worked
    if (client.authorized) {
        console.log("Connection authorized by a Certificate Authority.");
    }
    else {
        console.log("Connection not authorized: " + client.authorizationError);
        return;
    }
    // Send a friendly message
    client.on('data', (msgRaw) => {
        const msg = socket_1.processMessage(msgRaw);
        if (!msg) {
            return;
        }
        if (msg.type === socket_1.MessageType.input) {
            resolveClientInput(msg.data);
        }
    });
    client.on('error', (err) => {
        if (err.code === 'ECONNRESET') {
            reconnect();
        }
        else {
            console.error(err);
        }
    });
    client.on('end', function () {
        reconnect();
    });
    socket_1.writeMessageToSocket(client, socket_1.MessageType.client_init, profile_1.stripProfile(profile_1.getCurrentProfile())).catch((e) => {
        reconnect();
    });
    // TODO: heartbeat
    function write(buffer) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                client.write(buffer, (err) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(true);
                    }
                });
            });
        });
    }
}
exports.onConnect = onConnect;
let lastX;
let lastY;
let logging = false;
function resolveClientInput(input) {
    // console.log(input);
    if (screen_1.isKeyResult(input)) {
        if (input.type === 'ku' && input.keyCode === input_1.VIRTUAL_KEYS.RSHIFT) {
            logging = !logging;
        }
    }
    if (screen_1.isMouseResult(input) && input.type === 'mm') {
        if (lastX !== undefined && lastY !== undefined) {
            const dx = lastX - input.x;
            const dy = lastY - input.y;
            if (logging) {
                console.log(dx, dy);
            }
            screen_1.sendMouseInput({
                type: 'mm',
                x: -dx,
                y: -dy,
            });
        }
        lastX = input.x;
        lastY = input.y;
    }
}