"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const fs_1 = require("fs");
const _ = require("lodash");
const uuid_1 = require("uuid");
const yargs_1 = require("yargs");
exports.onConfigChange = new events_1.EventEmitter();
let reloading = false;
function getConfigPath() {
    if (yargs_1.argv.config) {
        return yargs_1.argv.config;
    }
    if (process.env.KEYPACT_CONFIG_PATH) {
        return process.env.KEYPACT_CONFIG_PATH;
    }
    return 'config/keypact-config.json';
}
exports.getConfigPath = getConfigPath;
function generateDefaultKeypactConfig() {
    return {
        id: uuid_1.v1(),
        tlsServerHost: '0.0.0.0',
        tlsServerPort: 42777,
        // tlsServer: true,
        certificate: {
            keyPath: 'config/test-key.pem',
            certPath: 'config/test-cert.pem',
        },
        // client: false,
        // clientServerAddress: '127.0.0.1',
        // clientServerPort: 42777,
        clientReconnectInterval: 5000,
        collatedInputInterval: 5,
        currentProfile: null,
        profiles: [],
    };
}
exports.generateDefaultKeypactConfig = generateDefaultKeypactConfig;
/**
 * Calls getConfig and monitors the config file for any changes.
 *
 * If there is a change that has been made to the config file,
 * onConfigChange will be called with 'change' event
 */
function monitorConfig() {
    return __awaiter(this, void 0, void 0, function* () {
        yield loadConfig();
        fs_1.watch(getConfigPath(), {
        // persistent: false,
        }, (eventType, filename) => __awaiter(this, void 0, void 0, function* () {
            if (eventType !== 'change' || reloading) {
                return;
            }
            reloading = true;
            console.log('Keypact configuration updated');
            // reload configuration from file
            const newConfig = yield loadConfig(false).catch((e) => {
                console.error('Error reading new config');
                console.error(e);
            }).then((r) => r);
            if (newConfig) {
                exports.onConfigChange.emit('change');
            }
            reloading = false;
        }));
        // console.log(config);
        return exports.savedConfig;
    });
}
exports.monitorConfig = monitorConfig;
function stopMonitoringConfig() {
    fs_1.unwatchFile(getConfigPath());
}
exports.stopMonitoringConfig = stopMonitoringConfig;
function getConfig() {
    if (!exports.savedConfig) {
        throw new Error('Config not set');
    }
    return exports.savedConfig;
}
exports.getConfig = getConfig;
/**
 * Reads the configuration from the config json file and
 * sets the config variable to its output.
 *
 * If the configuration file does not exist, it creates it with
 * the result from generateDefaultKeypactConfig
 *
 * @returns config variable
 *
 */
function loadConfig(update = true) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            exports.savedConfig = JSON.parse(yield fs_1.promises.readFile(getConfigPath(), 'utf8'));
        }
        catch (e) {
            if (e.code === 'ENOENT') {
                exports.savedConfig = generateDefaultKeypactConfig();
            }
            else {
                throw e;
            }
        }
        if (update) {
            yield updateConfig();
        }
        exports.config = processOverridenConfig();
        return exports.savedConfig;
    });
}
exports.loadConfig = loadConfig;
function setConfig(newConfig) {
    return __awaiter(this, void 0, void 0, function* () {
        yield fs_1.promises.writeFile(getConfigPath(), JSON.stringify(newConfig, null, 2));
        exports.savedConfig = newConfig;
        return newConfig;
    });
}
exports.setConfig = setConfig;
/**
 * Saves the current config state
 */
function saveConfig() {
    return __awaiter(this, void 0, void 0, function* () {
        reloading = true;
        yield setConfig(exports.config);
        reloading = false;
    });
}
exports.saveConfig = saveConfig;
function updateConfig() {
    return __awaiter(this, void 0, void 0, function* () {
        // place renames and removals here.
        yield setConfig(Object.assign(Object.assign({}, generateDefaultKeypactConfig()), exports.savedConfig));
    });
}
exports.updateConfig = updateConfig;
function processOverridenConfig() {
    exports.config = _.cloneDeep(exports.savedConfig);
    exports.config = _.merge(yargs_1.argv, exports.config);
    return exports.config;
}
exports.processOverridenConfig = processOverridenConfig;