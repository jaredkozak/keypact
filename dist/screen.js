"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const inputAddon = require('../build/Release/inputaddon.node');
function isMouseResult(result) {
    return result.type.startsWith('m');
}
exports.isMouseResult = isMouseResult;
function isKeyResult(result) {
    return result.type.startsWith('k');
}
exports.isKeyResult = isKeyResult;
function getScreenInformation() {
    const screens = inputAddon.Screen.getScreenInformation();
    for (const screen of screens) {
        screen.name = screen.name.replace(/([^0-9A-Za-z])/g, '');
    }
    return screens;
}
exports.getScreenInformation = getScreenInformation;
function sendMouseInput(input) {
    sendInput(input);
}
exports.sendMouseInput = sendMouseInput;
function sendKeyboardInput(input) {
    if (input.type === 'k') {
        sendInput(Object.assign({ type: 'kd' }, input));
        sendInput(Object.assign({ type: 'ku' }, input));
    }
    else {
        sendInput(input);
    }
}
exports.sendKeyboardInput = sendKeyboardInput;
function sendInput(input) {
    inputAddon.Screen.sendInput(input);
}
exports.sendInput = sendInput;
function listenInput(onInput) {
    inputAddon.Screen.listenInput(onInput);
}
exports.listenInput = listenInput;
function cleanupInput() {
    inputAddon.Screen.cleanupInput();
}
exports.cleanupInput = cleanupInput;
function blockInput(blockInput) {
    inputAddon.Screen.blockInput(blockInput);
}
exports.blockInput = blockInput;
function captureWindow(cb) {
    inputAddon.Screen.captureWindow(cb);
}
exports.captureWindow = captureWindow;