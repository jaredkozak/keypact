"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const config_1 = require("./config");
const screen_1 = require("./screen");
let lastKeyState = {};
let keyState = {};
let mouseState = {};
let lastX;
let lastY;
exports.onLocalInput = new events_1.EventEmitter();
exports.onCollatedLocalInput = new events_1.EventEmitter();
let collatedInputInterval;
function processMouseResult(result) {
    if (result.type === 'm.') {
        // unknown type
        console.log(result);
        return;
    }
    // console.log(result);
    result.dx = lastX !== undefined ? result.x - lastX : 0;
    result.dy = lastY !== undefined ? result.y - lastY : 0;
    lastX = result.x;
    lastY = result.y;
    exports.onLocalInput.emit('input', result);
}
function processKeyResult(result) {
    if (result.type === 'k.') {
        // unknown type
        console.log(result);
        return;
    }
    if (result.type === 'kd') {
        keyState[result.keyCode] = true;
    }
    else {
        keyState[result.keyCode] = false;
    }
    exports.onLocalInput.emit('input', result);
}
function startInputListener() {
    screen_1.listenInput((result) => {
        let type = '';
        try {
            if (screen_1.isMouseResult(result)) {
                type = 'mouse ';
                processMouseResult(result);
            }
            else if (screen_1.isKeyResult(result)) {
                type = 'keyboard ';
                processKeyResult(result);
            }
        }
        catch (e) {
            console.error(`Could not process ${type}result`);
            console.error(e);
            console.error(result);
        }
    });
    collatedInputInterval = setInterval(() => {
        // TODO: get collated input
        exports.onCollatedLocalInput.emit('input');
    }, config_1.config.collatedInputInterval);
}
exports.startInputListener = startInputListener;
function stopInputListener() {
    if (collatedInputInterval) {
        clearInterval(collatedInputInterval);
        collatedInputInterval = undefined;
    }
    screen_1.cleanupInput();
}
exports.stopInputListener = stopInputListener;
var VIRTUAL_KEYS;
(function (VIRTUAL_KEYS) {
    VIRTUAL_KEYS[VIRTUAL_KEYS["LSHIFT"] = 160] = "LSHIFT";
    VIRTUAL_KEYS[VIRTUAL_KEYS["RSHIFT"] = 161] = "RSHIFT";
    VIRTUAL_KEYS[VIRTUAL_KEYS["LCTRL"] = 162] = "LCTRL";
    VIRTUAL_KEYS[VIRTUAL_KEYS["RCTRL"] = 163] = "RCTRL";
    VIRTUAL_KEYS[VIRTUAL_KEYS["RALT"] = 165] = "RALT";
    VIRTUAL_KEYS[VIRTUAL_KEYS["LALT"] = 164] = "LALT";
    VIRTUAL_KEYS[VIRTUAL_KEYS["f"] = 70] = "f";
    VIRTUAL_KEYS[VIRTUAL_KEYS["r"] = 82] = "r";
    VIRTUAL_KEYS[VIRTUAL_KEYS["o"] = 79] = "o";
    VIRTUAL_KEYS[VIRTUAL_KEYS["g"] = 71] = "g";
})(VIRTUAL_KEYS = exports.VIRTUAL_KEYS || (exports.VIRTUAL_KEYS = {}));