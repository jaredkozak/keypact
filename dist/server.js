"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const tls = require("tls");
const config_1 = require("./config");
const input_1 = require("./input");
const profile_1 = require("./profile");
const socket_1 = require("./socket");
let server;
function startServer() {
    return __awaiter(this, void 0, void 0, function* () {
        const profile = profile_1.getCurrentProfile();
        if (server || !profile_1.isServerProfile(profile)) {
            return;
        }
        // TODO: generate certificates
        console.log('Starting TLS server');
        const options = {
            key: fs.readFileSync(config_1.config.certificate.keyPath),
            cert: fs.readFileSync(config_1.config.certificate.certPath),
        };
        server = tls.createServer(options, handleSocket);
        return new Promise((resolve, reject) => {
            try {
                server.listen(config_1.config.tlsServerPort, config_1.config.tlsServerHost, 0, resolve);
            }
            catch (e) {
                reject(e);
            }
        });
    });
}
exports.startServer = startServer;
function stopServer() {
    return __awaiter(this, void 0, void 0, function* () {
        if (!server) {
            return;
        }
        console.log('Stopping TLS server');
        return new Promise((resolve, reject) => server.close((err) => {
            if (err) {
                reject(err);
            }
            server = undefined;
            resolve();
        }));
    });
}
exports.stopServer = stopServer;
function restartServer() {
    return __awaiter(this, void 0, void 0, function* () {
        yield stopServer();
        yield startServer();
    });
}
exports.restartServer = restartServer;
function handleSocket(socket) {
    let clientName = socket.remoteAddress;
    let clientContext;
    const serverProfile = profile_1.getCurrentProfile();
    console.log(`Client connected: ${clientName}`);
    // Send a friendly message
    // blockInput(true);
    // Print the data that we received
    socket.on('data', function (raw) {
        const msg = socket_1.processMessage(raw);
        if (!msg) {
            return;
        }
        if (msg.type === socket_1.MessageType.client_init) {
            clientContext = profile_1.onClientInit(msg.data);
            if (!clientContext) {
                console.error(`Invalid client context: ${JSON.stringify(msg.data)}`);
                end();
                return;
            }
            onInit();
        }
    });
    socket.on('error', function (err) {
        if (err.code === 'ECONNRESET') {
            end();
        }
        else {
            console.log(err);
        }
    });
    // Let us know when the transmission is over
    socket.on('end', function () {
        end();
    });
    function end() {
        input_1.onLocalInput.removeListener('input', onInput);
        profile_1.profileEmitter.removeListener('change', onContextChanged);
        console.log(`Client ended: ${clientName}`);
        socket.destroy();
    }
    function onInit() {
        // register input listener to send client input information
        input_1.onLocalInput.addListener('input', onInput);
        // Let the clients know about any updates to the servers current context
        profile_1.profileEmitter.addListener('change', onContextChanged);
        // send client information about the server context
        socket_1.writeMessageToSocketSafe(socket, socket_1.MessageType.server_init, profile_1.stripProfile(serverProfile));
        // TODO register heartbeat
        config_1.saveConfig().catch(console.error);
    }
    function onInput(input) {
        const profile = profile_1.getCurrentProfile();
        if (!profile || !clientContext || !profile_1.isServerProfile(profile)) {
            return;
        }
        if (profile.state.mouseAndKeyboardProfile !== clientContext.id) {
            return;
        }
        // console.log(input);
        socket_1.writeMessageToSocketSafe(socket, socket_1.MessageType.input, input);
    }
    function onContextChanged() {
        socket_1.writeMessageToSocketSafe(socket, socket_1.MessageType.server_init, profile_1.stripProfile(serverProfile));
    }
}
exports.handleSocket = handleSocket;