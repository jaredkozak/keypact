"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pem = require("pem");
pem.config({
    pathOpenSSL: 'C:\\Program Files\\OpenSSL-Win64\\bin\\openssl.exe',
});
pem.createCertificate({
    days: 1,
    commonName: 'Saphire',
    selfSigned: true,
    clientKeyPassword: 'jared@kozakatak.com:12345678',
}, (err, keys) => {
    console.log(err);
    console.log(keys);
});